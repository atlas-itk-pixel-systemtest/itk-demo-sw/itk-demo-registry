# pyregistry
is a wrapper for the service registry endpoints. It contains several functions to access the HTTP endpoints in python code. For more information on the functionalities, please refer to the SR API.

## Installation
To install this package and use it simply install it via pip (preferably from inside a venv). This will also enable you to run the CLI by running pyconfigdb from you terminal.
```bash
pip install pyregistry --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```
To upgrade run:
```bash
pip install --upgrade pyregistry --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```

# ITK-DEMO-REGISTRY

The itk-demo-registry microservice is the API-Version of the [ServiceRegistry](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/pypi-packages/service-registry). It provides an fastAPI interface to the main functions provided by the [ServiceRegistry](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/pypi-packages/service-registry) python package, which provides useful function to set one or multiple key/value pairs or read one or multiple key/value pairs or watch changes of one or multiple key.

Slides giving an overview of the itk-demo-service microservice can be found [here](https://indico.cern.ch/event/1373836/contributions/5774492/attachments/2803264/4891093/SR-API.pdf).

## Setup
Instructions on the setup of the itk-demo-registry container can be found under quick setup. 
A more generell microservice setup guide can be found [here](https://demi.docs.cern.ch/tutorials/microservice_setup).

## How to use
The itk-demo-registry can be used via the Swagger UI, which allows the user to visualize and interact with the API's resources without having any of the implementation logic in place. This UI can be accessed by combining the Ip-adress of the dockercompose.yaml for example 5111 and the "/api/docs" suffix to an URL like: 
```
http://hostname:5111/api/docs 
```
Now all the tools of the ServiceRegistry-API can be easily used as an HTTP-API.

## Dump

All existing key/value pairs will be returned as an htmlText, if the Swagger UI is accesed without any additional path-suffix. For the example ip-adress of earlier the URL would look like this:
```
http://hostname:5111/
```

### Registrator

The itk-demo-registry container automatically registers other docker containers based on the labels in their docker compose files. For each service to be registered with a container one namespace has to be introduced. Each label uses this namespace to define to which service it belongs to.


#### General labels
| Label | Example | Usage |
|----------|----------|----------|
| description | Interface to FELIX hardware | Description of the service |
| category | Microservice | Category of the service |
| health | /health | Endpoint that should be used to access health information of service |
#### Labels needed for URL generation

| Label | Example | Usage |
|----------|----------|----------|
| host | wupp-charon.cern.ch | Fully qualified! hostname of the PC hosting the container |
| port | 8000 | Container internal! port of the service (only needed when multiple ports are exposed) |
| url_prefix | /api | Prefix for the service of the container |
| url_postfix | /docs | Postfix for the SWAGGER UI |
| protocol | http | Protocol used to access service |

The url is automatically constructed using the information from the labels above. If no hostport is given, the port is automatically detected from docker. The url is constructed in the following way:

- API/UI: protocol://host:hostport/url_prefix
- SWAGGER UI: protocol://host:hostport/url_prefix/url_postfix

#### Example

An exmplary list of labels for the felix container containing the UI and API services looks like this. Note that the ${STACK} variable is read from the env vars and typically contains something like sr1_lls.
```
labels:
    - demi.${STACK}.felix.api.host=${HOST}
    - demi.${STACK}.felix.api.port=8000
    - demi.${STACK}.felix.api.url_prefix=
    - demi.${STACK}.felix.api.url_postfix=/docs
    - demi.${STACK}.felix.api.health=/health
    - demi.${STACK}.felix.api.description=Interface to FELIX hardware
    - demi.${STACK}.felix.api.protocol=http
    - demi.${STACK}.felix.api.category=Microservice

    - demi.${STACK}.felix.ui.host=${HOST}
    - demi.${STACK}.felix.ui.port=3000
    - demi.${STACK}.felix.ui.category=Microservice-UI
    - demi.${STACK}.felix.ui.description=FELIX UI
    - demi.${STACK}.felix.ui.protocol=http
```