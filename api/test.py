from itk_demo_registry.registry.adapter.redis_adapter import RedisAdapter
from itk_demo_registry.registry.adapter.etcd_adapter import EtcdAdapter
import time

sr = EtcdAdapter()
#sr = RedisAdapter()


# einfach
sr.put("Fire", "Water")
print("put & get: ", sr.get("Fire"))
# mehrfach
sr.put_many({"dominyk": "yk", "dominic": "ic", "domeínik": "ik"})
print(f"Suffix search: {sr.get_suffix('nyk' ,'dom')}")
print("put_many & get_prefix: ", sr.get_prefix("dom"))
# alle
# print("get_all: ",sr.get_prefix(""))
# watch
cancel = sr.watch("key1", print)

# Simulate some changes to the keys
for i in range(10):
    time.sleep(1)
    sr.put("key1", f"v{i}")
    # sr.put('key2', f'v{i}')
    sr.delete("key1")

    # watcher.redis_client.publish('key3', f'v{i}')
    if i == 5:
        cancel()
