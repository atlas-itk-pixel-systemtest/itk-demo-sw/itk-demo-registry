from itk_demo_registry.registry.exceptions import SRException, SRKeyError
import threading

from etcd3gw.exceptions import Etcd3Exception

from itk_demo_registry.registry.detect_etcd import detect_etcd
from itk_demo_registry.registry.adapter.base_adapter import BaseAdapter

import logging
from rich.logging import RichHandler
from itk_demo_registry.registry.config import log_level

logging.basicConfig(level=log_level, format="%(message)s", handlers=[RichHandler(level=log_level, show_time=False)])
log = logging.getLogger("rich")
log.level = log_level


class EtcdAdapter(BaseAdapter):
    def __init__(self, host="localhost", port=2379, namespace=""):
        self.namespace = namespace
        self.client = detect_etcd(host, port)
        # client = Etcd3Client(host=etcd_host, port=etcd_port, api_path="/v3/")
        # try:
        #     client.status()
        #     self.client = client
        #     if self.client is None:
        #         raise Exception
        # except Exception as e:
        if not self.client:
            raise SRException(
                "ETCD not available",
                "Could not be detected",
                # f"ETCD as a service is not running under the specified address "
                # f"'{etcd_host}:{etcd_port}'.",
            )  # from e

    def _get_full_key(self, key):
        return f"{self.namespace}{key}"

    def get_apis(self, key_prefix):
        values = self.get_prefix(key_prefix=key_prefix)
        apis = []

        for key, value in values.items():
            if key.endswith("/api_name") and value != "":
                url_key = key[:-9] + "/url"
                if url_key in values:
                    apis.append({"name": value, "url": values[url_key]})
        return apis

    def put(self, key, value, ttl=-1):
        try:
            lease = None
            if ttl != -1:
                lease = self.client.lease(ttl)
            self.client.put(key=self._get_full_key(key).encode("utf-8"), value=value.encode("utf-8"), lease=lease)
        except Etcd3Exception as e:
            raise SRException(
                "ETCD Error",
                f"Unable to create save key-value pair '{key}' - '{value}'. " f"Message: {e.__class__.__name__} - {e.detail_text}",
            ) from e

    def get_prefix(self, key_prefix="", remove_prefix=False):
        try:
            kv_pairs = {}

            if key_prefix == "":
                for v, k in self.client.get_all():
                    key = str(k["key"], encoding="utf-8")[len(self._get_full_key("")) :]
                    value = str(v, encoding="utf-8")
                    if remove_prefix:
                        kv_pairs[key.replace(key_prefix, "")] = value
                    else:
                        kv_pairs[key] = value
            else:
                for v, k in self.client.get_prefix(self._get_full_key(key_prefix)):
                    key = str(k["key"], encoding="utf-8")[len(self._get_full_key("")) :]
                    value = str(v, encoding="utf-8")
                    if remove_prefix:
                        kv_pairs[key.replace(key_prefix, "")] = value
                    else:
                        kv_pairs[key] = value
            # if not kv_pairs:
            #     raise SRKeyError(f"No key with prefix '{key_prefix}' exists.")
            return kv_pairs
        except Etcd3Exception as e:
            raise SRException(
                "ETCD Error",
                f"Unable to find key_prefix: '{key_prefix}' " f"Message: {e.__class__.__name__} - {e.detail_text}",
            ) from e

    def put_many(self, kv_pairs, ttl=-1):
        try:
            lease = None
            if kv_pairs:
                if ttl != -1:
                    lease = self.client.lease(ttl)
                for key, value in kv_pairs.items():
                    self.client.put(key=self._get_full_key(key).encode("utf-8"), value=value.encode("utf-8"), lease=lease)
        except Etcd3Exception as e:
            raise SRException(
                "ETCD Error",
                f"Unable to create save key-value pair '{key}' - '{value}'. " f"Message: {e.__class__.__name__} - {e.detail_text}",
            ) from e

    def delete_prefix(self, prefix):
        try:
            self.client.delete_prefix(prefix)
        except Etcd3Exception as e:
            raise SRException(
                "ETCD Error",
                f"Unable to delete keys with prefix '{prefix}'. " f"Message: {e.__class__.__name__} - {e.detail_text}",
            ) from e

    def watch(self, key, callback, prefix=False):
        if prefix:
            watcher, watch_cancel = self.client.watch_prefix(self._get_full_key(key))
        else:
            watcher, watch_cancel = self.client.watch(self._get_full_key(key))

        def wrapped_callback(evt):
            event_type = None
            key = None
            value = None
            if "type" not in evt:
                if "kv" in evt:
                    event_type = "PUT"
                else:
                    event_type = "UNKNOWN"
            else:
                event_type = evt["type"]
            if "kv" in evt:
                try:
                    key = evt["kv"]["key"].decode()
                except KeyError:
                    key = None
                try:
                    value = evt["kv"]["value"].decode()
                except Exception:
                    value = None
            callback(event_type, key, value)

        def thread_with_watcher():
            for event in watcher:
                wrapped_callback(event)

        threading.Thread(target=thread_with_watcher).start()

        return watch_cancel

    def get(self, key):
        try:
            res = self.client.get(key=self._get_full_key(key))
            if len(res) == 0:
                raise SRKeyError(f"The key '{key}' does not exist.")
            value_str = str(res[0], encoding="utf-8")
            try:
                return int(value_str)
            except ValueError:
                pass
            try:
                return float(value_str)
            except ValueError:
                pass
            return value_str

        except Etcd3Exception as e:
            raise SRException(
                "ETCD Error",
                f"Unable to obtain value for key '{key}'. " f"Message: {e.__class__.__name__} - {e.detail_text}",
            ) from e

    def delete(self, key):
        try:
            self.client.delete(key=self._get_full_key(key))

        except Etcd3Exception as e:
            raise SRException(
                "ETCD Error",
                f"Unable to delete key '{key}'. " f"Message: {e.__class__.__name__} - {e.detail_text}",
            ) from e

    def backend_alive(self):
        try:
            self.client.status()
            return True
        except Exception as e:
            log.error(e)
            return False


if __name__ == "__main__":
    sr = EtcdAdapter()
    apis = sr.get_apis("demi")
    a = 1
