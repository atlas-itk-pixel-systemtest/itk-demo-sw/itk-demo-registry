from config_checker import BaseConfig, EnvSource
from typing import Literal

import logging
from rich.logging import RichHandler


class RegistryConfig(BaseConfig):
    backend_host: str | None = None
    backend_port: str | None = None
    backend_adapter: Literal["etcd", "redis"] = "etcd"
    log_level: str = logging._levelToName[logging.INFO]

    CONFIG_SOURCES = [EnvSource(allow_all=True, file=".env")]


config = RegistryConfig()
valid_log_level = True
try:
    log_level = logging._nameToLevel[config.log_level]
except KeyError:
    original_log_level = config.log_level
    log_level = logging.INFO
    valid_log_level = False

logging.basicConfig(level=log_level, format="%(message)s", handlers=[RichHandler(level=log_level, show_time=False)])
log = logging.getLogger("rich")
log.level = log_level
if not valid_log_level:
    log.warning(f"{original_log_level} is not a valid log level. Use one of {logging._nameToLevel.keys()}")
