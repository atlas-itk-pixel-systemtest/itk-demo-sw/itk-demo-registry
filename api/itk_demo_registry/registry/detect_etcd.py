import os
import socket

# import sys

from etcd3gw.client import Etcd3Client

from dotenv import load_dotenv

import logging
from rich.logging import RichHandler
from itk_demo_registry.registry.config import log_level

logging.basicConfig(level=log_level, format="%(message)s", handlers=[RichHandler(level=log_level, show_time=False)])
log = logging.getLogger("rich")
log.level = log_level

load_dotenv()


def in_docker():
    """Check if running in docker.
    (Apparently using environment variables is preferred... tbd)
    """
    path = "/proc/self/cgroup"
    return os.path.exists("/.dockerenv") or os.path.isfile(path) and any("docker" in line for line in open(path))


def try_etcd(host, port):
    try:
        client = Etcd3Client(host=host, port=port, api_path="/v3/")
        if client.status():
            log.info(f"Connection at {host}:{port} ok")
            return client
    # except ConnectionFailedError as e:
    except Exception:
        log.debug("Could not connect to etcd")
        # log.exception(e)
        return None


def detect_etcd(host, port):
    if host:
        log.info(f"Trying host: {host}:{port}")
        client = try_etcd(host, port)
        if client:
            log.info(f"Connected to etcd on {host}:{port}")
            return client
        log.info("Could not connect to etcd. Trying default hosts...")

    default_port = 2379
    if not in_docker():
        etcd_host_list = [
            "etcd",
            socket.gethostname(),
            "localhost",
            "127.0.0.1",
        ]

        for host in etcd_host_list:
            log.debug(f"Trying host: '{host}'")
            client = try_etcd(host, default_port)
            if client:
                log.info(f"Connected to etcd on {host}:{default_port}")
                return client
    else:
        log.info("Seems like we're in a Docker container...")
        etcd_docker_host_list = [
            "etcd",
            socket.gethostname(),
            "demitools-etcd-1",
            "host.docker.internal",
            "host.containers.internal",
        ]

        for host in etcd_docker_host_list:
            log.debug(f"Trying: '{host}'")
            client = try_etcd(host, default_port)
            if client:
                log.info(f"Connected to etcd on {host}:{default_port}")
                return client

    return None
