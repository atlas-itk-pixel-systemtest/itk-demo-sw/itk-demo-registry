import sys
import click
from itk_demo_registry.registry.exceptions import SRException, SRKeyError
from itk_demo_registry.registry.registry import ServiceRegistry


class InputError(Exception):
    def __init__(self, msg):
        self.title = "Input Error"
        self.msg = msg
        super().__init__(f"{self.title} - {self.msg}")


@click.group("registry_cli")
def registry_cli():
    pass


@registry_cli.command()
@click.argument("kv_pair", nargs=-1, type=str)
@click.option(
    "-t",
    "--ttl",
    type=int,
    default=-1,
    help="Lifetime of registration. Indefinite lifetime if not specified.",
)
@click.option(
    "-ns",
    "--namespace",
    type=str,
    default="",
    help="Namespace of registration.",
    show_default=True,
)
@click.option(
    "-eh",
    "--etcd_host",
    type=str,
    default="localhost",
    help="Host of ETCD instance.",
    show_default=True,
)
@click.option(
    "-ep",
    "--etcd_port",
    type=int,
    default=2379,
    help="Port of ETCD instance.",
    show_default=True,
)
def put(kv_pair, ttl, namespace, etcd_host, etcd_port):
    """
    Set given key value pair.

    kv_pair: two arguments in the format KEY1 VALUE1
    """
    if len(kv_pair) != 2:
        raise InputError("KV_PAIR have to be two arguments in the format KEY1 VALUE1")

    try:
        sr = ServiceRegistry(namespace=namespace)
        sr.put(kv_pair[0], kv_pair[1], ttl=ttl)
    except SRException as e:
        click.echo("Failed to register service because an error occurred: ", file=sys.stderr)
        click.echo(f"{e.title} - {e.msg}", file=sys.stderr)
        exit(1)

    click.echo("Operation successful.")


@registry_cli.command("put_many", short_help="Puts key-value pairs into the service registry")
@click.argument("kv_pairs", nargs=-1, type=str)
@click.option(
    "-t",
    "--ttl",
    type=int,
    default=-1,
    help="Lifetime of registration. Indefinite lifetime if not specified.",
)
@click.option(
    "-ns",
    "--namespace",
    type=str,
    default="",
    help="Namespace of registration.",
    show_default=True,
)
@click.option(
    "-eh",
    "--etcd_host",
    type=str,
    default="localhost",
    help="Host of ETCD instance.",
    show_default=True,
)
@click.option(
    "-ep",
    "--etcd_port",
    type=int,
    default=2379,
    help="Port of ETCD instance.",
    show_default=True,
)
def put_many(kv_pairs, ttl, namespace, etcd_host, etcd_port):
    """
    Set given key value pairs.

    kv_pair: an even number of arguments in the format KEY1 VALUE1 KEY2 VALUE2 ...
    """
    if len(kv_pairs) % 2 != 0:
        raise InputError("KV_PAIRS have to be an even number of arguments in the format " "KEY1 VALUE1 KEY2 VALUE2 ...")
    formatted_kv_pairs = {kv_pairs[2 * i]: kv_pairs[2 * i + 1] for i in range(int(len(kv_pairs) / 2))}

    try:
        sr = ServiceRegistry(namespace=namespace)
        sr.put_many(formatted_kv_pairs, ttl=ttl)
    except SRException as e:
        click.echo("Failed to register service because an error occurred: ", file=sys.stderr)
        click.echo(f"{e.title} - {e.msg}", file=sys.stderr)
        exit(1)

    click.echo("Operation successful.")


@registry_cli.command()
@click.argument("key", type=str)
@click.option(
    "-ns",
    "--namespace",
    type=str,
    default="",
    help="Namespace of registration.",
    show_default=True,
)
@click.option(
    "-eh",
    "--etcd_host",
    type=str,
    default="localhost",
    help="Host of ETCD instance.",
    show_default=True,
)
@click.option(
    "-ep",
    "--etcd_port",
    type=int,
    default=2379,
    help="Port of ETCD instance.",
    show_default=True,
)
def get(key, namespace, etcd_host, etcd_port):
    """
    Get value of a given key.

    key: key to search for
    """
    try:
        sr = ServiceRegistry(namespace=namespace)
        value = sr.get(key)
        click.echo(f"{key}: {value}")

    except SRKeyError:
        click.echo(f"Key '{key}' not found.")

    except SRException as e:
        click.echo(
            f"Failed to get value for key '{key}' " f"because an error occurred: ",
            file=sys.stderr,
        )
        click.echo(f"{e.title} - {e.msg}", file=sys.stderr)
        exit(1)


@registry_cli.command("get_many", short_help="Get all keys starting with a given prefix")
@click.argument(
    "prefix",
    type=str,
    default="",
)
@click.option(
    "-ns",
    "--namespace",
    type=str,
    default="",
    help="Namespace of registration.",
    show_default=True,
)
@click.option(
    "-eh",
    "--etcd_host",
    type=str,
    default="localhost",
    help="Host of ETCD instance.",
    show_default=True,
)
@click.option(
    "-ep",
    "--etcd_port",
    type=int,
    default=2379,
    help="Port of ETCD instance.",
    show_default=True,
)
def get_many(prefix, namespace, etcd_host, etcd_port):
    """
    Get all values of a keys starting with a given prefix.

    prefix: prefix to search for (leave empty for all keys)
    """
    try:
        sr = ServiceRegistry(namespace=namespace)
        kv_pairs = sr.get_prefix(prefix)

        for key, value in kv_pairs.items():
            click.echo(f"{key}: {value}")

    except SRKeyError:
        click.echo(f"No key starting with prefix '{prefix}' found.")

    except SRException as e:
        click.echo(
            f"Failed to get value for key_prefix '{prefix}' " f"because an error occurred: ",
            file=sys.stderr,
        )
        click.echo(f"{e.title} - {e.msg}", file=sys.stderr)
        exit(1)


@registry_cli.command("delete", short_help="Delete given key")
@click.argument("key", type=str)
@click.option(
    "-ns",
    "--namespace",
    type=str,
    default="",
    help="Namespace of registration.",
    show_default=True,
)
@click.option(
    "-eh",
    "--etcd_host",
    type=str,
    default="localhost",
    help="Host of ETCD instance.",
    show_default=True,
)
@click.option(
    "-ep",
    "--etcd_port",
    type=int,
    default=2379,
    help="Port of ETCD instance.",
    show_default=True,
)
def delete(key, namespace, etcd_host, etcd_port):
    """
    Delete given key.

    key: key to delete
    """
    try:
        sr = ServiceRegistry(namespace=namespace)
        sr.delete(key)
        click.echo(f"Deleted key '{key}'.")
    except SRException as e:
        click.echo(
            f"Failed to delete key {key} because an error occurred: ",
            file=sys.stderr,
        )
        click.echo(f"{e.title} - {e.msg}", file=sys.stderr)
        exit(1)


@registry_cli.command("delete_many", short_help="Delete all keys starting with given prefix")
@click.argument("key_prefix", type=str)
@click.option(
    "-ns",
    "--namespace",
    type=str,
    default="",
    help="Namespace of registration.",
    show_default=True,
)
@click.option(
    "-eh",
    "--etcd_host",
    type=str,
    default="localhost",
    help="Host of ETCD instance.",
    show_default=True,
)
@click.option(
    "-ep",
    "--etcd_port",
    type=int,
    default=2379,
    help="Port of ETCD instance.",
    show_default=True,
)
def delete_many(key_prefix, namespace, etcd_host, etcd_port):
    """
    Deletes all key starting with a given prefix.

    prefix: prefix to delete
    """
    try:
        sr = ServiceRegistry(namespace=namespace)
        sr.delete_prefix(key_prefix)
        click.echo(f"Deleted all keys starting with '{key_prefix}'.")
    except SRException as e:
        click.echo(
            f"Failed to delete keys with key_prefix {key_prefix} because an error occurred: ",
            file=sys.stderr,
        )
        click.echo(f"{e.title} - {e.msg}", file=sys.stderr)
        exit(1)


def main():
    registry_cli(prog_name="registry_cli")


if __name__ == "__main__":
    main()
