from itk_demo_registry.registry.adapter.etcd_adapter import EtcdAdapter
from itk_demo_registry.registry.adapter.redis_adapter import RedisAdapter
from itk_demo_registry.registry.adapter.base_adapter import BaseAdapter
from itk_demo_registry.registry.config import config


class ServiceRegistry(BaseAdapter):
    adapter: BaseAdapter = None

    def __init__(self, namespace="", backend_host=config.backend_host, backend_port=config.backend_port, backend_adapter=config.backend_adapter):
        if backend_adapter == "etcd":
            self.adapter = EtcdAdapter(host=backend_host, port=backend_port, namespace=namespace)
        elif backend_adapter == "redis":
            self.adapter = RedisAdapter(host=backend_host, port=backend_port, namespace=namespace)
        else:
            raise Exception(f"Unknown backend adapter {backend_adapter}")

    def get_suffix(self, suffix: str, prefix: str = ""):
        return self.adapter.get_suffix(suffix, prefix)

    def put(self, key: str, value: str, ttl=-1):
        return self.adapter.put(key, value, ttl)

    def get_prefix(self, key_prefix: str = "", remove_prefix=False):
        return self.adapter.get_prefix(key_prefix, remove_prefix)

    def put_many(self, kv_pairs: str, ttl=-1):
        return self.adapter.put_many(kv_pairs, ttl)

    def delete_prefix(self, prefix: str):
        return self.adapter.delete_prefix(prefix)

    def watch(self, key: str, callback):
        return self.adapter.watch(key, callback)

    def get(self, key: str):
        return self.adapter.get(key)

    def delete(self, key: str):
        return self.adapter.delete(key)

    def backend_alive(self):
        return self.adapter.backend_alive()
