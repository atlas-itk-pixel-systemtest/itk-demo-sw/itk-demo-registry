class SRException(Exception):
    def __init__(self, title, msg):
        self.title = title
        self.msg = msg
        super().__init__(f"{title} - {msg}")


class SRKeyError(SRException):
    def __init__(self, msg):
        super().__init__("Key Error", msg)
