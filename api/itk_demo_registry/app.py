from itk_demo_registry.routes import router, dump_router
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from itk_demo_registry.registry.registry import ServiceRegistry


def create_app():
    app = FastAPI(
        title="ServiceRegistry API",
        description="HTTP api to the service registry",
        version="3.1.2",
        docs_url="/api/docs",  # custom location for swagger UI
    )
    app.include_router(router)
    app.include_router(dump_router)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],  # Allows all origins
        allow_credentials=True,
        allow_methods=["*"],  # Allows all methods
        allow_headers=["*"],  # Allows all headers
    )

    sr = ServiceRegistry()
    if not sr.backend_alive():
        raise Exception("backend is not running")

    return app


# if __name__ == "__main__":
#     if len(sys.argv) > 1:
#         # arg, if any, is the desired port number
#         port = int(sys.argv[1])
#         assert port > 1024
#     else:
#         port = os.getuid()
#     app.debug = True
#     app.run("0.0.0.0", port)
