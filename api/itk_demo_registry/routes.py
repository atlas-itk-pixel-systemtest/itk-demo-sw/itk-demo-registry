from itk_demo_registry.registry.registry import ServiceRegistry
from itk_demo_registry.registry.exceptions import SRKeyError, SRException
from itk_demo_registry.bodys import SetBody, SetManyBody, SetBodySidebar
import queue
from collections import defaultdict
import uuid
from fastapi import APIRouter, Query, Request, HTTPException
from fastapi.responses import HTMLResponse, StreamingResponse
import asyncio
import logging
from rich.logging import RichHandler
from itk_demo_registry.registry.config import log_level

logging.basicConfig(level=log_level, format="%(message)s", handlers=[RichHandler(level=log_level, show_time=False)])
log = logging.getLogger("rich")
log.level = log_level


router = APIRouter(prefix="/api")
dump_router = APIRouter()


sr = ServiceRegistry()


@router.get(
    "/dump/",
    summary="dump all",
    description="dumps all keys and values from the SR",
    response_class=HTMLResponse,
)
async def dump():
    try:
        kv_dump = sr.get_prefix("")
    except SRKeyError:
        raise HTTPException(status_code=404, detail="Service Registry is empty")
    htmlLines = []
    htmlLines = [f"<tr><td>{k}</td><td>:</td><td>{kv_dump[k]}</td></tr>" for k in kv_dump.keys()]

    if not kv_dump:
        htmlText = "<p>no keys found</p>"
    else:
        htmlLines = []
        htmlLines = [f"<tr><td>{k}</td><td>:</td><td>{kv_dump[k]}</td></tr>" for k in kv_dump.keys()]
        htmlText = '<table style="font-family: monospace">' + "\n".join(htmlLines) + "</table>"

    return htmlText


dump_router.add_api_route("/", dump, methods=["GET"], include_in_schema=False, response_class=HTMLResponse)


@router.get("/health/", summary="check if registry is available")
async def health():
    backend_state = sr.backend_alive()
    if not backend_state:
        raise HTTPException(status_code=404, detail="backend not available")
    else:
        return {"state": "active", "status": 200}


@router.post(
    "/set/",
    tags=["set"],
    summary="set a kv pair",
    description="Set a key/value pair und return it",
)
async def put(kv: SetBody):
    sr.put(kv.key, kv.value)
    output = {kv.key: kv.value}
    return output


@router.post(
    "/set/sidebar/",
    tags=["set"],
    summary="set a sidebar kv pair",
    description="Set a sidebar key/value pair und return it",
)
async def put_sidebar(kv: SetBodySidebar):
    url = sr.get(f"{kv.namespace}/url")
    value = f"{url}/dashboardSideBarContent/{kv.identifier}"

    sr.put(f"demi/dashboard/files/{kv.serial}", value)
    output = {f"demi/dashboard/files/{kv.serial}": value}
    return output


@router.get(
    "/get/",
    tags=["get"],
    summary="get a key",
    description="Returns corresponding value to a key",
)
async def get(key: str = Query(default=None, example="your_key")):
    try:
        value = sr.get(key)
        output = {key: value}
    except SRKeyError:
        raise HTTPException(status_code=404, detail=f"key {key} not found")
    return output


@router.get(
    "/get_prefix/",
    tags=["get"],
    summary="search for keys with a given prefix",
    description="Returns all key/value-pairs, where the key begins with the given prefix.",
)
async def get_prefix(
    prefix: str = Query(default="", example="your_prefix"),
    remove_prefix: bool = Query(default=False, example=False),
):
    try:
        output = sr.get_prefix(prefix, remove_prefix)
    except SRKeyError:
        raise HTTPException(status_code=404, detail=f"no key found for prefix {prefix}")
    return output


@router.post(
    "/set_many/",
    tags=["set"],
    summary="Put multiple key/value pairs into the SR",
    description="multiple key/value pairs can be put into the SR by using a dict",
)
async def set_many(kv_dict: SetManyBody):
    # kv_dict = {}

    # for kv_pair in input.rsplit(";"):
    #     kv = kv_pair.rsplit(":")
    #     kv_dict[kv[0]]=kv[1]

    sr.put_many(kv_dict.kv_dict)
    return kv_dict.kv_dict


class MessageAnnouncer:
    def __init__(self):
        self.listeners = defaultdict(list)
        self.queue_dict = {}
        self.key_cancel_dict = {}
        self.cleanup_tasks = set()

    def listen(self, key, id):
        # logging.info(self.key_cancel_dict, self.listeners)
        if id in self.queue_dict:
            q = self.queue_dict[id]

        else:
            q = queue.Queue(maxsize=5)
            self.queue_dict[id] = q
        if key not in self.key_cancel_dict:
            cancel = sr.watch(key, callback)
            self.key_cancel_dict[key] = cancel
        self.listeners[key].append(q)
        return q

    def announce(self, msg, key):
        for i in reversed(range(len(self.listeners[key]))):
            try:
                self.listeners[key][i].put_nowait(msg)
            except queue.Full:
                del self.listeners[key][i]
        if self.listeners[key] == []:
            self.key_cancel_dict[key]()
            del self.key_cancel_dict[key]

    def cancel(self, key, id):
        if id in self.queue_dict:
            q = self.queue_dict[id]
            del self.queue_dict[id]
            for i in range(len(self.listeners[key])):
                if self.listeners[key][i] == q:
                    log.debug(f"deleting queue from listeners for key: {key}")
                    del self.listeners[key][i]
                    break
            if self.listeners[key] == []:
                log.debug(f"deleting key: {key} from listeners")
                self.key_cancel_dict[key]()
                del self.key_cancel_dict[key]


def format_sse(data: str, event=None) -> str:
    msg = f"data: {data}\n\n"
    if event is not None:
        msg = f"event: {event}\n{msg}"
    return msg


announcer = MessageAnnouncer()

# @app.route('/watch')
# def watch_form():
#     return flask.render_template('watch.html')


def callback(event_type, key, value):
    msg = format_sse(data=str(value), event=key)
    announcer.announce(msg=msg, key=key)


async def stream(keys, request, messages, id):
    try:
        for key in keys:
            value = sr.get(key)
            yield format_sse(data=str(value), event=key)
    except SRKeyError:
        pass
    while True:
        try:
            msg = messages.get_nowait()
            yield msg
        except queue.Empty:
            await asyncio.sleep(0.1)


async def loop(request, keys, id):
    while True:
        if await request.is_disconnected():
            for key in keys:
                announcer.cancel(key, id)
            break


@router.get(
    "/watch/",
    tags=["watch"],
    summary="watch keys",
    description="Reports all changes that effects the given keys",
)
async def watch(request: Request, keys: str = Query(default=None, example="your_key_1;your_key_2")):
    keys = keys.rsplit(";")
    id = uuid.uuid4()
    for key in keys:
        messages = announcer.listen(key, id)  # returns a queue.Queue

    task = asyncio.create_task(loop(request, keys, id))

    announcer.cleanup_tasks.add(task)
    task.add_done_callback(announcer.cleanup_tasks.discard)

    # log.debug(f"Returning response for keys: {keys}")

    return StreamingResponse(stream(keys, request, messages, id), media_type="text/event-stream")


@router.delete("/delete_prefix/", tags=["delete"], summary="delete all keys with a given prefix", description="Deletes all keys with a given prefix")
async def delete_prefix(prefix: str = Query(default="", example="yourKey")):
    try:
        output = sr.delete_prefix(prefix)
    except SRException:
        raise HTTPException(status_code=404, detail=f"Unable to delete prefix '{prefix}'")
    return output


@router.delete("/delete/", tags=["delete"], summary="delete a key", description="Deletes the given key")
async def delete(key: str = Query(default="", example="yourKey")):
    try:
        output = sr.delete(key)
    except SRException:
        raise HTTPException(status_code=404, detail=f"Unable to delete key '{key}'")
    return output
