import yaml
from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi
from itk_demo_registry.app import create_app


def generate_openapi_yaml(app: FastAPI, file_path: str):
    openapi_schema = get_openapi(
        title=app.title,
        version=app.version,
        openapi_version=app.openapi_version,
        description=app.description,
        routes=app.routes,
    )
    with open(file_path, "w") as file:
        yaml.dump(openapi_schema, file, default_flow_style=False)


if __name__ == "__main__":
    app = create_app()
    generate_openapi_yaml(app, "openapi.yaml")
