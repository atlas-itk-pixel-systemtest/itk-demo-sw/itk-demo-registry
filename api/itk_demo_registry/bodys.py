from pydantic import BaseModel
from typing import Union


class SetBody(BaseModel):
    key: str
    value: str

    model_config = {"json_schema_extra": {"examples": [{"key": "your_key", "value": "your_value"}]}}


class SetBodySidebar(BaseModel):
    serial: Union[str, int]
    namespace: str
    identifier: str

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "serial": "dev0_serial",
                    "namespace": "demi/default/felix/api",
                    "identifier": "dev0",
                }
            ]
        }
    }


class SetManyBody(BaseModel):
    kv_dict: dict

    model_config = {"json_schema_extra": {"examples": [{"kv_dict": {"key1": "value1", "key2": "value2"}}]}}
