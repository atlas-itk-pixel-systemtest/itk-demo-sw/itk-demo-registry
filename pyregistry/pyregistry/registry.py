from pyregistry.exceptions import SRResponseError, SRConnectionError
import json
from requests import post, get, delete
from requests.exceptions import ConnectionError


class ServiceRegistry:
    def __init__(
        self,
        srUrl="http://localhost:5111/api",
        headers={"content-type": "application/json", "accept": "application/json"},
    ):
        self.headers = headers
        srUrl = srUrl.strip("/")
        self.srUrl = srUrl

        if not self.health():
            raise SRConnectionError(srUrl)

    def _standard_post(self, endpoint, body, parameter, default_msg):
        try:
            response = post(
                url=f"{self.srUrl}{endpoint}",
                params=parameter,
                data=json.dumps(body),
                headers=self.headers,
            )
        except ConnectionError:
            raise SRConnectionError(self.srUrl)

        if not response.status_code // 100 == 2:
            raise SRResponseError(response, default_msg)

        return response.json()

    def _standard_get(self, endpoint, parameter, default_msg):
        try:
            response = get(f"{self.srUrl}{endpoint}", params=parameter, headers=self.headers)
        except ConnectionError:
            raise SRConnectionError(self.srUrl)

        if not response.status_code // 100 == 2:
            raise SRResponseError(response, default_msg)

        return response.json()

    def _standard_delete(self, endpoint, parameter, default_msg):
        try:
            response = delete(f"{self.srUrl}{endpoint}", params=parameter, headers=self.headers)
        except ConnectionError:
            raise SRConnectionError(self.srUrl)

        if not response.status_code // 100 == 2:
            raise SRResponseError(response, default_msg)

        return response.json()

    # High-level endpoints

    def health(self) -> bool:
        """
        Check health of service registry

        Returns
        -------
        True if successfull
        """
        try:
            self._standard_get("/health", {}, "")
        except (SRConnectionError, SRResponseError):
            return False

        return True

    def set(self, key: str, value: str) -> bool:
        """
        Adds a key to the sr.

        Parameters
        ----------
        key: str
            Key to be added.
        value: str
            Value to be added.

        Returns
        -------
            True if successfull
        """
        self._standard_post(
            "/set",
            {"key": key, "value": value},
            {},
            "Could not add key.",
        )
        return True

    def set_sidebar(self, identifier: str, key: str, namespace: str) -> bool:
        """
        Set a sidebar key/value pair

        Parameters
        ----------
        identifier: str
            identifier of the component
        serial: str
            serial of the component
        namespace: str
            namespace of the component

        Returns
        -------
            True if successfull
        """
        self._standard_post(
            "/set/sidebar",
            {"identifier": identifier, "serial": key, "namespace": namespace},
            {},
            "Could not add key.",
        )
        return True

    def set_many(self, kv_dict: dict[str, str]) -> bool:
        """
        Adds multiple keys to the sr.

        Parameters
        ----------
        kv_dict: dict[str, str]
            dict containing key-value pairs to be added.

        Returns
        -------
            True if successfull
        """
        self._standard_post(
            "/set_many",
            {"kv_dict": kv_dict},
            {},
            "Could not add keys.",
        )
        return True

    def get(self, key: str) -> str:
        """
        Gets a key from the sr

        Parameters
        ----------
        key: str
            key to get

        Returns
        -------
            value of the key
        """
        res = self._standard_get(
            "/get",
            {"key": key},
            "Could not get key.",
        )
        return res[key]

    def get_prefix(self, prefix: str, remove_prefix: bool) -> dict[str, str]:
        """
        Gets all keys starting with a prefix from the sr

        Parameters
        ----------
        prefix: str
            prefix to get
        remove_prefix: bool
            defines if the prefix should be removed from all keys

        Returns
        -------
            dict with key value pairs
        """
        res = self._standard_get(
            "/get_prefix",
            {"prefix": prefix, "remove_prefix": remove_prefix},
            "Could not get key.",
        )
        return res

    def delete_prefix(self, prefix: str) -> bool:
        """
        Deletes all keys with the given prefix

        Parameters
        ----------
        prefix: str
            prefix to delete

        Returns
        -------
        True if successfull
        """
        try:
            self._standard_delete("/delete_prefix", {"prefix": prefix}, "Could not delete prefix")
        except (SRConnectionError, SRResponseError):
            return False

        return True

    def delete(self, key: str) -> bool:
        """
        Deletes the given key

        Parameters
        ----------
        key: str
            key to delete

        Returns
        -------
        True if successfull
        """
        try:
            self._standard_delete("/delete", {"key": key}, "Could not delete key")
        except (SRConnectionError, SRResponseError):
            return False

        return True


if __name__ == "__main__":
    pass
