from requests import JSONDecodeError
from rcf_response.exceptions import RcfException


class SRException(RcfException):
    def __init__(self, msg, title="SR Error", status=500, instance=None, ext=None):
        super().__init__(status, title, msg, instance=instance, ext=ext)


class SRResponseError(SRException):
    def __init__(self, response, default_msg):
        status = response.status_code
        title = "SRError"
        try:
            data = response.json()
            title += f": {data}"
            msg = f"{default_msg} SR response: {data['detail']} ({response.url})"
            ext = {"sr-response": data}
            super().__init__(msg, title=title, status=status, ext=ext)
        except (JSONDecodeError, KeyError):
            super().__init__(default_msg, title=title, status=status)


class SRConnectionError(SRException):
    def __init__(self, srUrl):
        title = "SRError"
        msg = f"No connection could be established to the service registry at URL {srUrl}."
        super().__init__(msg, title=title, status=404)
