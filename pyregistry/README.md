### Publish

```shell
 poetry config repositories.pyregistry "https://gitlab.cern.ch/api/v4/projects/175451/packages/pypi/"

 poetry publish -r pyregistry -u username -p $(cat ~/.gitlab-token)
```

## Installation
To install this package and use it simply install it via pip (preferably from inside a venv). This will also enable you to run the CLI by running pyconfigdb from you terminal.
```bash
pip install pyregistry --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```
To upgrade run:
```bash
pip install --upgrade pyregistry --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```