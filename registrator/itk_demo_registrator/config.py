from config_checker import BaseConfig, EnvSource

# from .pyregistry.registry import ServiceRegistry
from pyregistry.registry import ServiceRegistry

import logging
from rich.logging import RichHandler


class Registrator_Config(BaseConfig):
    sr_url: str = "http://localhost:5111/api"
    log_level: str = logging._levelToName[logging.INFO]

    CONFIG_SOURCES = [EnvSource(allow_all=True, file=".env")]


config = Registrator_Config()
valid_log_level = True
try:
    log_level = logging._nameToLevel[config.log_level]
except KeyError:
    original_log_level = config.log_level
    log_level = logging.INFO
    valid_log_level = False

logging.basicConfig(level=log_level, format="%(message)s", handlers=[RichHandler(level=log_level, show_time=False)])
log = logging.getLogger("rich")
log.level = log_level
if not valid_log_level:
    log.warning(f"{original_log_level} is not a valid log level. Use one of {logging._nameToLevel.keys()}")

sr = ServiceRegistry(config.sr_url)
if not sr.health():
    log.error(f"Service registry at {config.sr_url} not healthy")
    import sys

    sys.exit(1)

log.info(f"Connected to Service Registry at {config.sr_url}")
