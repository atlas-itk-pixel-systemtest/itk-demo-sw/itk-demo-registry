import sys
import time
from datetime import datetime

from itk_demo_registrator.config import sr, log_level

import docker
import docker.errors

import logging
from rich.logging import RichHandler

logging.basicConfig(level=log_level, format="%(message)s", handlers=[RichHandler(level=log_level, show_time=False)])
log = logging.getLogger("rich")
log.level = log_level

try:
    client = docker.from_env()
except Exception as e:
    log.error(" Docker not available!")
    log.exception(e)
    sys.exit(1)


def len_sr(sr=sr, prefix="demi"):
    kv_pairs = sr.get_prefix(prefix, False)

    return len(kv_pairs.items())


def dump_sr(sr=sr, prefix="demi", of="rawdata"):
    kv_pairs = sr.get_prefix(prefix)

    with open(of, "w") as _of:
        for key, value in kv_pairs.items():
            print(f"{key}:{value}")
            _of.write(f"{key}:{value}\n")


def get_container_list():
    container_list = []
    for container_adress in client.containers.list():
        container = client.containers.get(container_adress.short_id)
        container_list.append(container)
    return container_list


def build_url(kv_pairs, namespace):
    url = kv_pairs[namespace + "/host"] + ":" + kv_pairs[namespace + "/hostport"]

    if namespace + "/url_prefix" in kv_pairs:
        url = url + kv_pairs[namespace + "/url_prefix"]

    if namespace + "/protocol" in kv_pairs:
        url = kv_pairs[namespace + "/protocol"] + "://" + url
    else:
        url = "http://" + url

    # if namespace + "/category" in kv_pairs:
    #     if kv_pairs[namespace + "/category"] == "Microservice":
    #         kv_pairs[namespace + "/api_url"] = url
    #         url = url + "/ui"

    kv_pairs[namespace + "/url"] = url

    return kv_pairs


def read_container_info(container, label_prefix, save_env_vars=False):
    data = container.attrs
    if label_prefix[-1] != ".":
        label_prefix += "."
    kv_pairs_dict = {}

    labels = data["Config"]["Labels"]
    # print(labels)

    try:
        service = labels["com.docker.compose.service"]
    except Exception:
        log.warning(f" {container.name}: No compose service found!")
        return None

    log.debug(f" {container.name}: Service '{service}'")

    found_prefixed_label = False
    namespaces = []

    # Reads docker labels
    for key, value in labels.items():
        if key.startswith(label_prefix):
            log.debug(f" {container.name}: Found label '{key}'='{value}'")
            found_prefixed_label = True

            # add key only if well-formed {demi}.{...}.{service}.{...}
            namespace_exists = False
            for namespace in namespaces:
                key2 = key.replace(".", "/")
                if key2.startswith(namespace):
                    namespace_exists = True
                    kv_pairs_dict[namespace][key2] = value
                    break

            if not namespace_exists:
                namespace = ".".join(key.split(".")[:-1])
                namespace = namespace.replace(".", "/")
                log.info(f" {container.name}: Namespace '{namespace}'")
                namespaces.append(namespace)
                kv_pairs_dict[namespace] = {}
                key2 = key.replace(".", "/")
                kv_pairs_dict[namespace][key2] = value

    if not found_prefixed_label:
        log.info(f" {container.name}: No '{label_prefix}' labels")
        return None

    if len(namespaces) == 0:
        log.warning(f" {container.name}: Could not determine any service namespace!")
        return None

    # add url to keys
    # TODO: add support for binding the same port to multiple hostports
    for namespace, kv_pairs in kv_pairs_dict.items():
        if namespace + "/host" in kv_pairs and kv_pairs[namespace + "/host"] != "":
            log.debug(" Let's build some urls!!!")
            if namespace + "/hostport" in kv_pairs:
                log.debug(" Building directly")
                kv_pairs = build_url(kv_pairs, namespace)
            else:
                log.debug(" Building from docker ports")
                ports = container.ports
                # print(ports)

                port_labeled = False
                for key, value in kv_pairs.items():
                    if key[-5:] == "/port":
                        for port, binds in ports.items():
                            log.debug("   " + str(port) + " - " + str(binds))
                            if value == port.split("/")[0]:
                                binding = binds[0]
                                if binding["HostIp"].count(".") == 3:
                                    hostport = binding["HostPort"]
                                    kv_pairs[namespace + "/hostport"] = hostport
                                    log.debug("       Building URL!")
                                    kv_pairs = build_url(kv_pairs, namespace)
                                    port_labeled = True
                                    break
                    if port_labeled:
                        break

                if not port_labeled:
                    if len(ports) > 1:
                        log.warning(
                            f"{container.name}: Multiple exposed ports found, but no docker label defining which port to use for which servcie. Using the first one."
                        )

                    for port, binds in ports.items():
                        log.debug("   " + str(port) + " - " + str(binds))
                        # print(k, v)

                        if not binds:
                            log.debug("     No binds!")
                            # to do: handle multiple unbound ports
                            kv_pairs[namespace + "/port"] = port
                            continue
                        else:
                            log.debug("     Some binds!")
                            for binding in binds:
                                log.debug("       Binding bind: " + str(binding))
                                # to do: handle multiple bound ports

                                # Only save IPv4 port
                                if binding["HostIp"].count(".") == 3:
                                    hostport = binding["HostPort"]
                                    kv_pairs[namespace + "/hostport"] = hostport
                                    log.debug("       Building URL!")
                                    kv_pairs = build_url(kv_pairs, namespace)
                                    break

                            # internport = list(data["HostConfig"]["PortBindings"].keys())[0]
                            # try:
                            #     if  internport in data["NetworkSettings"]["Ports"]:
                            #         for count,Network_ports in enumerate(
                            # data["NetworkSettings"]["Ports"][internport], start=1):
                            #             Network_port = Network_ports["HostPort"]
                            #             kv_pairs["port"+ str(count)] = Network_port
                            #     else:
                            #         print("internport does not match")
                            # except ValueError:
                            #         pass

                            # kv_pairs["status"] = data["State"]["Status"]

    if save_env_vars:
        for env_variable in data["Config"]["Env"]:
            x = env_variable.rsplit("=")
            kv_pairs[x[0]] = x[1]

    # pprint(kv_pairs, expand_all=True)
    return kv_pairs_dict


def write_container_info(container, kv_pairs_dict: dict):
    if not kv_pairs_dict:
        log.warning(f" Container {container.name} is missing demi-keys.")
    else:
        for namespace, kv_pairs in kv_pairs_dict.items():
            try:
                sr.set_many(kv_pairs)
                log.info(f"Container {container.name} was registered.")
            except (KeyError, TypeError):
                log.error(f" Error in Registration of Container {container.name}.")


def running_service_readout(label_prefix="demi"):
    container_list = get_container_list()
    for container in container_list:
        log.debug(container.name)
        kv_pairs_dict = read_container_info(container=container, label_prefix=label_prefix)
        write_container_info(container, kv_pairs_dict)

    date = datetime.today().strftime("%d.%m.%Y")
    time = datetime.now().strftime("%H:%M:%S")
    log.info(f" All running containers registered on {date} at {time}.")


def event_watcher(timestamp=None, label_prefix="demi"):
    try:
        for event in client.events(since=timestamp, decode=True):
            if event["Type"] == "container":
                # log.debug(event)
                try:
                    container_ID = event["Actor"]["Attributes"]["container"]
                except Exception as e:
                    try:
                        container_ID = event["id"]
                    except Exception:
                        log.exception(e)
                        continue

                if "Action" in event:
                    action = event["Action"]
                    # if action.startswith("exec") or action == "disconnect":
                    #     continue

                    if action == "die" or action == "kill":
                        for key in event["Actor"]["Attributes"].keys():
                            if key.find("demi") == 0:
                                key = key.replace(".", "/")
                                delete_prefix = key[: key.rfind("/")]
                                try:
                                    sr.delete_prefix(delete_prefix)
                                except Exception as e:
                                    if not str(e).startswith("SRError"):
                                        raise e
                                    log.warning(f"Trying to delete {event.prefix}. Not found in registry.")
                        log.info(f" {event['Actor']['Attributes']['name']} deleted")

                    elif action == "start" or action == "restart":
                        tries = 2
                        container = None
                        while tries > 0:
                            try:
                                container = client.containers.get(container_ID)
                            except docker.errors.NotFound:
                                tries -= 1
                                time.sleep(2)
                                if tries == 0:
                                    log.error(f"Container {container_ID} not found.")
                                    break
                            else:
                                break
                        if not container:
                            continue
                        try:
                            if not list(container.ports.values())[0]:
                                time.sleep(2)
                                container = client.containers.get(container_ID)
                        except IndexError:
                            pass
                        kv_pairs_dict = read_container_info(container=container, label_prefix=label_prefix)
                        if kv_pairs_dict:
                            for namespace, kv_pairs in kv_pairs_dict.items():
                                try:
                                    sr.set_many(kv_pairs)
                                    log.info(f"Container {namespace} was registered.")
                                except (KeyError, TypeError):
                                    log.error(f" Error in Registration of Container {namespace}.")
                        else:
                            log.debug(f" Container {container.name} is missing demi-keys.")

                else:
                    log.debug(" no action")

    except docker.errors.APIError as e:
        log.exception(e)


def main():
    timestamp = time.time_ns()
    running_service_readout()
    event_watcher(timestamp)


if __name__ == "__main__":
    main()
