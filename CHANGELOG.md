# Changelog

All notable changes[^1] to the `itk-demo-registry`

This repo is "living at head" which means tags correspond to the overall repo.
All subrepos/workspaces, all packages in the package registry and all container images in the image registry follow the repo tag.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.0.4] - 2024-11-15
 - registrator
    - Prevent crashing when a container can not be found

## [4.0.3] - 2024-11-08
 - registrator
    - Bumped pyregistry version
    - (failed to) Prevent crashing when a container can not be found

## [4.0.2] - 2024-11-08
 - Added registrator to ci to build docker images

## [4.0.1] - 2024-11-08
 - Adjusted image url for registrator

## [4.0.0] - 2024-11-08
 - Separated registrator from registry. The registrator communicates over HTTP with the registry.
 - Added 'delete' and 'delete_prefix' endpoints to registry.

## [3.1.2] - 2024-09-24
 - Fixed bug in registrator script that crashed script if no container info could be read
 - Get-Prefix return empty dict instead of error if nothing is found

## [3.1.1] - 2024-08-26
 - Automatically remove trailing / in sr url

## [3.1.0] - 2024-08-21
 - Restructuring
 - Added pyregistry
 - Bugfix

## [3.0.0] - 2024-08-14
 - Moved code from service-registry package to microservice
 - Added Adapter classes, itk-demo-registry uses etcd adapter (for now)
 - Added openapi file

## [2.2.0] - 2024-04-24
 - Added endpoint to register dashboard sidebar content
 - Changed type of health endpoint to int

## [2.1.0] - 2024-04-12
 - Added support for multiple services (namespaces) per container
    - Specify which of the exposed ports to use for which service of a container with the docker label port 

## [2.0.7] - 2024-04-09
 - Fixed format of additional dump route (was returned as json instead of HTML)

## [2.0.6] - 2024-03-05
 - Added additional dump route

## [2.0.5] - 2024-03-05
 - Bugfix in registry: delete all felix labels to remove from dashboard

## [2.0.4] - 2024-02-14
 - Fixed erros being returned with statuscode 200

## [2.0.3] - 2024-02-06
 - Bugfix in garbage collection

## [2.0.2] - 2024-01-30
 - Bugfix in cleanup of watch requests

## [2.0.1] - 2024-01-24
 - Added cleanup of watch requests

## [2.0.0] - 2024-01-10
 - Changed framework from flask to fast api

## [1.0.8] - 2024-01-09
 - Fixed bug causing errors to be returned with 200 status code

## [1.0.7] - 2024-01-09
 - Fixed bug in set_many endpoint

## [1.0.6] - 2024-01-05
 - Upgrade to s6-overlay 3.1.6.2
    - Fixes container not stopping on CMD exit
 
## [1.0.5] - 2024-01-04
 - Changed container to use pip instead of poetry

## [1.0.4] - 2023-12-19
 - Added landing page
 - Stop flask server if etcd is not available



